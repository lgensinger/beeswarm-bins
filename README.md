# Beeswarm Bins

ES6 d3.js clustered circles in binned sets visualization.

## Install

```bash
# install package
npm install @lgv/beeswarm-bins
```

## Data Format

The following values are the expected input data structure; id can be omitted.

```json
[
    {
        "label": "xyz",
        "value": 1,
        "size": 5
    },
    {
        "label": "abc",
        "value": 3,
        "size": 10
    }
]
```

## Use Module

```bash
import { BeeswarmBins } from "@lgv/beeswarbins";

// have some data
let data = [
    {
        "label": "xyz",
        "value": 1,
        "size": 5
    },
    {
        "label": "abc",
        "value": 3,
        "size": 10
    }
];

// initialize
const bb = new BeeswarmBins(data);

// render visualization
bb.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_BIN_COUNT` | integer | number of bins |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| bubble circle | `bubble-click` | on click |
| bubble circle | `bubble-mouseover` | on hover |
| bubble circle | `bubble-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-bubble` | circle element |
| `lgv-beeswarm-bins` | top-level svg element |
| `lgv-node` | circle/text group element |
| `lgv-bubble-label` | circle text label element |
| `lgv-bubble-label-partial` | individual stacked lines of circle text label element |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd beeswarm-bins

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const bb = new BeeswarmBins(data);
# add bb.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
