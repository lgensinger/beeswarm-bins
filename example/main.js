import packageJson from "../package.json";
import { BeeswarmBins } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

let data = [
    { label: "a", value: 1, size: 2 },
    { label: "b", value: 2, size: 3 },
    { label: "c", value: 4, size: 1 },
    { label: "d", value: 1, size: 2 },
    { label: "e", value: 3, size: 5 },
    { label: "f", value: 10, size: 4 }
];

// get elements
let container = document.getElementsByTagName("figure")[0];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // determine configs
    let width = container.offsetWidth;
    let height = width*0.75;
    let radiusExtent = [10,20];

    // initialize
    const bb = new BeeswarmBins(data,width,height,null,["bin 1","bin 2","bin 3"],radiusExtent);

    // render visualization
    bb.render(container);

}

// load document
document.onload = startup(data,container);

// attach events
container.outputContainer = outputContainer;
container.addEventListener("bubble-click",processEvent);
container.addEventListener("bubble-mouseover", processEvent);
container.addEventListener("bubble-mouseout", processEvent);