import { ForceLayout as FL } from "@lgv/visualization-chart";
import { forceCollide, forceSimulation, forceX, forceY } from "d3-force";

/**
 * ForceLayout is a data-bound layout abstraction for a force layout chart.
 * @param {array} data - objects where each represents a path in the hierarchy
 */
class ForceLayout extends FL {
    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.axisScale = params ? params.axisScale : null;
        this.valueScale = params ? params.valueScale : null;
        this.y = params ? params.y : null;

    }

    /**
     * Construct layout.
     * @returns A d3 force simulation function.
     */
    get layout() {
        return forceSimulation(this.data)
            .force("collide", forceCollide().strength(1).radius(d => this.rScale(d.size) * 1.5))
            .force("x", forceX().x(d => this.axisScale(this.valueScale(d.value))))
            .force("y", forceY().y(this.y))
            .stop();
    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e) {
        return {
            bin: this.valueScale(d.value),
            id: this.extractId(d),
            label: this.extractLabel(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            xy: [e.clientX, e.clientY]
        };

    }

    /**
     * Get radius from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum radius value.
     */
    extractRadius(d) {
        return d.size;
    }

    /**
     * Get x from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum value.
     */
    extractX(d) {
        return d.value;
    }


}

export { ForceLayout };
export default ForceLayout;
