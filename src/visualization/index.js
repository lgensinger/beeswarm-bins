import { Beeswarm } from "@lgv/beeswarm";
import { ChartLabel, largestDivisor } from "@lgv/visualization-chart";
import { extent, max } from "d3-array";
import { axisBottom } from "d3-axis";
import { scaleBand, scaleLinear, scalePoint, scaleQuantile } from "d3-scale";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { configuration, configurationLayout } from "../configuration.js";
import { ForceLayout as FL } from "../layout/index.js";

/**
 * BeeswarmBins is a grouping visualization with clustered values.
 * @param {bins} array - strings where each represents a bin label
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {Class} ForceLayout - javascript class representing the layout abstraction
 * @param {integer} height - artboard height
 * @param {array} radiusExtent - [min,max] of radii as floats
 * @param {integer} width - artboard width
 * @param {array} xExtent - [min,max] of x scale domain as floats
 * @param {array} yExtent - [min,max] of y scale domain as floats
 */
class BeeswarmBins extends Beeswarm {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, ForceLayout=null, bins=null, radiusExtent=null, xExtent=null, yExtent=null, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, ForceLayout ? ForceLayout : new FL(data), radiusExtent, xExtent, yExtent, label, name);

        // update self
        this.bin = null;
        this.bins = bins;
        this.binCount = bins ? bins.length : largestDivisor(max(this.data.map(d => d.value)));
        this.binBackground = null;
        this.classBin = `${label}-bin`;
        this.classBinBackground = `${label}-bin-background`;

        // have to update layout functions after initialization
        this.Data.axisScale = this.axisScale;
        this.Data.valueScale = this.valueScale;
        this.Data.rScale = this.rScale;
        this.Data.y = this.yScale.range()[1] + (this.domainY / 2);

    }

    /**
     * Construct axis.
     * @returns A d3 axis function.
     */
    get axisBottom() {
        return axisBottom(this.axisScale)
            .tickFormat((d,i) => this.bins ? this.bins[i] : d);
    }

    /**
     * Construct axis scale so that ticks are centered in each bin.
     * @returns A d3 scale function.
     */
    get axisScale() {
        return scalePoint()
            .domain(this.binScale.domain())
            .range([this.xScale.range()[0] + this.halfBin, this.xScale.range()[1] - this.halfBin]);
    }

    /**
     * Construct bin scale.
     * @returns A d3 scale function.
     */
    get binScale() {
        return scaleBand()
            .domain([...Array(this.binCount).keys()].map(d => d + 1))
            .range(this.xScale.range())
            .round(true);
    }

    /**
     * Determine half bin size.
     * @returns A float representing half the width of a bin as determined by binScale.
     */
    get halfBin() {
        return this.binScale.bandwidth() / 2;
    }

    /**
     * Construct value scale to organize values into bin center points.
     * @returns A d3 scale function.
     */
    get valueScale() {
        return scaleQuantile()
            .domain(this.data.map(d => d.value))
            .range(this.binScale.domain());
    }

    /**
     * Construct y scale.
     * @returns A d3 scale function.
     */
    get yScale() {
        return scaleLinear()
            .domain([1,1])
            .range([this.height - this.maximumSize, this.maximumSize]);
    }

    /**
     * Position and minimally style x-axis line in SVG dom element.
     */
    configureAxisX() {
        this.axisX
            .transition().duration(1000)
            .attr("class", this.classAxisX)
            .attr("transform", `translate(0,${this.yScale.range()[1]})`)
            .call(this.axisBottom);
    }

    /**
     * Position and minimally style bin rect in SVG dom element.
     */
    configureBinBackgrounds() {
        this.binBackground
            .transition().duration(1000)
            .attr("class", this.classBinBackground)
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", this.binScale.bandwidth())
            .attr("height", this.domainY)
            .attr("fill", "lightgrey");
    }

    /**
     * Position and minimally style bin group in SVG dom element.
     */
    configureBins() {
        this.bin
            .transition().duration(1000)
            .attr("class", this.classBin)
            .attr("data-label", (d,i) => this.bins ? this.bins[i] : d)
            .attr("data-value", d => d)
            .attr("transform", d => `translate(${this.binScale(d)},${this.yScale.range()[1]})`);
    }

    /**
     * Construct bin background in HTML DOM.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateBinBackgrounds(domNode) {
        return domNode
            .selectAll(`.${this.classBinBackground}`)
            .data(d => [d])
            .join(
                enter => enter.append("rect"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct bin group in HTML DOM.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateBins(domNode) {
        return domNode
            .selectAll(`.${this.classBin}`)
            .data(this.binScale.domain())
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate bins
        this.bin = this.generateBins(this.content);
        this.configureBins();

        // generate bin backgound
        this.binBackground = this.generateBinBackgrounds(this.bin);
        this.configureBinBackgrounds();

        // generate group for each bubble
        this.node = this.generateNodes(this.content);
        this.configureNodes();

        // generate bubbles
        this.bubble = this.generateBubbles(this.node);
        this.configureBubbles();
        this.configureBubbleEvents();

        // simulate force to generate static positions of bubbles
        for (var i = 0; i < 360; ++i) this.Data.layout.tick();

        // update node positions
        this.node
            .transition().duration(1000)
            .attr("transform", d => `translate(${d.x},${d.y})`);

        // generate axis
        this.axisX = this.generateAxisX(this.artboard);
        this.configureAxisX();

        // generate labels
        this.label = this.generateLabels(this.node);
        this.configureLabels();

        // generate label partials so they stack
        this.labelPartial = this.generateLabelPartials(this.label);
        this.configureLabelPartials();

    }

};

export { BeeswarmBins };
export default BeeswarmBins;
